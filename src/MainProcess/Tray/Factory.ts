import {MainProcess_App} from '../App';
import {MainProcess_Tray_Default} from './Default';
import {MainProcess_Tray_OSX} from './OSX';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
export class MainProcess_Tray_Factory {
    protected static _tray: MainProcess_Tray_Default;
    public static getInstance() {
        return MainProcess_Tray_Factory._tray
            ? MainProcess_Tray_Factory._tray
            : MainProcess_Tray_Factory._create();
    }

    protected static _create() {
        let app = MainProcess_App.getInstance();
        let params = {parent: app};
        switch (true) {
            case app.isOSX():
                return new MainProcess_Tray_OSX(params);
            default:
                return new MainProcess_Tray_Default(params);
        }
    }
}

