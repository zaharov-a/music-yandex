import {BrowserWindow, BrowserWindowConstructorOptions, screen} from 'electron';
import {MainProcess_App} from '../MainProcess/App';
import path = require('path');
import {MainProcess_Store} from '../MainProcess/Store';
interface IWindowMainConfig {
    width: number,
    height: number,
    x: number,
    y: number,
    url: string,
    display: number,
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
export class Window_Main {

    protected static _self: Window_Main;
    protected _window: BrowserWindow;
    protected _parent: MainProcess_App;

    /**
     * 
     * @returns {Window_Main}
     */
    public static getInstance(): Window_Main {
        return Window_Main._self ? Window_Main._self : (Window_Main._self = new Window_Main({parent: MainProcess_App.getInstance()}));
    }

    public getBrowserWindow() {
        return this._window;
    }

    public getSettings() {
        let position = this._window.getPosition();
        let data = {
            url: this._window.webContents.getURL(),
            x: position[0],
            y: position[1],
            //display: 0,
        };
        if (!this._parent.isMini()) {
            let size = this._window.getSize();
            data['width'] = size[0];
            data['height'] = size[1];
        }
        return data;
    }

    public mini() {
        let screenSize = screen.getPrimaryDisplay().size;
        this._window.setContentSize(460, 125);
//        this._window.setPosition(screenSize.width - 460, 10);
    }


    protected constructor(params: {parent: MainProcess_App}) {
        let _self = this;
        let cfg: IWindowMainConfig = MainProcess_Store.getInstance().get('window');

        this._window = new BrowserWindow(this._getWindowOptions(cfg));
        this._parent = params.parent;

        this._window.on('close', function (event: Event) {
            if (!_self._parent.willQuitApp()) {
                event.preventDefault();
                _self._window.hide();
            }
        });

        this._window.on('closed', function () {
            // Сброс объекта окна, обычно нужен когда вы храните окна
            // в массиве, это нужно если в вашем приложении множество окон, 
            // в таком случае вы должны удалить соответствующий элемент.
            //        let config = {
            //            size: mainWindow.getSize(),
            //            position: mainWindow.getPosition(),
            //        }
            //
            //        fs.writeFile("/tmp/test", JSON.stringify(config), function (err)
            //        {
            //            if (err) {
            //                return console.log(err);
            //            }
            //
            //            console.log("The file was saved!");
            //        });

            _self._window = null;
        });

        this._window.loadURL(cfg.url);
        this._window.webContents.on('dom-ready', function () {
            _self._window.webContents.executeJavaScript('binder.bind()');
        });
    }

    protected _getWindowOptions(cfg: IWindowMainConfig) {
        let data: BrowserWindowConstructorOptions = {
            width: cfg.width,
            height: cfg.height,
            webPreferences: {
                nodeIntegration: false,
                contextIsolation: false,
                preload: path.join(global['APPLICATION_PATH'], 'App', 'preload.js'),
            },
        };

        if (cfg.x !== null && cfg.y !== null) {
            data.x = cfg.x;
            data.y = cfg.y;
        }
        return data;
    }
}