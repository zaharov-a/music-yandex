/**
 * Поехали
 */
import {MainProcess_App} from './MainProcess/App';
import path = require('path');

global['APPLICATION_PATH'] = path.join(__dirname, '..');

MainProcess_App.getInstance().run();