import {BrowserWindow} from 'electron';
import {MainProcess_App} from '../MainProcess/App';

export class ExternalAPI_MainProcess {
    public EVENT_STATE: 'state';
    public EVENT_TRACK: 'track';
    public EVENT_VOLUME: 'volume';
    public EVENT_PROGRESS: 'progress';
    public EVENT_CONTROLS: 'controls';
    public SHUFFLE_ON: true;
    public SHUFFLE_OFF: false;
    public REPEAT_NONE: false;
    public REPEAT_ALL: true;
    public REPEAT_ONE: 1;

    protected _window: BrowserWindow;

    public static getIstance() {
        return new ExternalAPI_MainProcess({window: MainProcess_App.getInstance().getMainWindow().getBrowserWindow()});
    }

    public constructor(params: {window: BrowserWindow}) {
        this._window = params.window;
    }

    public play() {
        this._execute('play()');
    }

    public next() {
        this._execute('next()');
    }

    public prev() {
        this._execute('prev()');
    }

    public togglePause() {
        this._execute('togglePause()');
    }

    public toggleShuffle() {
        this._execute('toggleShuffle()');
    }

    public toggleMute() {
        this._execute('toggleMute()');
    }

    public toggleRepeat(state: string) {
        this._execute(`toggleRepeat(${state})`)
    }

    protected _execute(action: string) {
        this._window.webContents.executeJavaScript(`externalAPI.${action}`);
    }
}


