/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import {app, ipcMain} from 'electron';
import path = require('path');
import {Window_Main} from '../Window/Main';
import {MainProcess_Tray_Factory} from './Tray/Factory';
import {MainProcess_Tray_Default} from './Tray/Default';
import {MainProcess_Store} from './Store';
import {ExternalAPI_MainProcess} from '../ExternalAPI/MainProcess';
import {IExternalAPI_TrackInfo, IExternalAPI_ControlsInfo} from '../ExternalAPI/interfaces';

export class MainProcess_App {
    protected static _self: MainProcess_App;
    protected _mainWindow: Window_Main;
    protected _tray: MainProcess_Tray_Default;
    protected _binder: ExternalAPI_MainProcess;
    protected _willQuitApp = false;
    protected _isPlatform: {[property: string]: boolean} = {};
    protected _settings: MainProcess_Store;

    protected constructor() {
        app.setName('YaM');
        app.setPath('userData', path.join(app.getPath('appData'), app.getName()));
        this._settings = MainProcess_Store.getInstance();
    }

    public static getInstance() {
        return MainProcess_App._self ? MainProcess_App._self : (MainProcess_App._self = new MainProcess_App());
    }

    public getMainWindow() {
        return this._mainWindow;
    }

    public getExternalAPI() {
        return this._binder;
    }

    public isMini() {
        return this._tray.isChecked('mini');
    }

    public run() {
        let _self = this;

        app.on('ready', () => {
            _self._mainWindow = Window_Main.getInstance();
            _self._binder = new ExternalAPI_MainProcess({window: _self._mainWindow.getBrowserWindow()});
            _self._tray = MainProcess_Tray_Factory.getInstance();
            
            _self._tray.setMini(_self._settings.get('mini'));

            _self._initEvents();
        });

        app.on('before-quit', function () {
            _self._willQuitApp = true;
            _self._settings.setProperties({
                window: _self._mainWindow.getSettings(),
                mini: _self.isMini(),
            }).save();
        });

        //        app.on('window-all-closed', function () {
        //            // В OS X это характерно для приложений и их меню,
        //            // чтобы оставаться активными, пока пользователь явно не завершит работу 
        //            // при помощи Cmd + Q
        //            //if (process.platform != 'darwin') {
        //            app.quit();
        //            //}
        //        });

        this._isPlatform = {
            'linux': process.platform == 'linux',
            'windows': process.platform == 'win32',
            'osx': process.platform == 'darwin',
        };
    }

    public quit() {
        this._willQuitApp = true;
        app.quit();
    }

    public willQuitApp() {
        return this._willQuitApp;
    }

    public isOSX() {
        return this._isPlatform['osx'];
    }

    public isLinux() {
        return this._isPlatform['linux'];
    }

    public isWindows() {
        return this._isPlatform['windows'];
    }

    protected _initWindow() {
        this._mainWindow = Window_Main.getInstance();
    }

    protected _initEvents() {
        let _self = this;
        ipcMain.on('state', function (event, data) {
            _self._tray.setImage(data ? 'play' : 'pause');
            _self._tray.setPlayingState(data);
        });

        ipcMain.on('volume', function (event, data) {

        });

        ipcMain.on('track', function (event, data: IExternalAPI_TrackInfo) {
            _self._tray.setToolTip(data.title);
        });

        ipcMain.on('controls', function (event, data: IExternalAPI_ControlsInfo) {
            _self._tray.setShuffleState(data.shuffle);
            _self._tray.setRepeatState(data.repeat);
        });

        ipcMain.on('progress', function (event, data) {
            _self._mainWindow.getBrowserWindow().setProgressBar(data || -1);
        });
    }
}