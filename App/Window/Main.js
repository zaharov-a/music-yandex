"use strict";
exports.__esModule = true;
var electron_1 = require("electron");
var App_1 = require("../MainProcess/App");
var path = require("path");
var Store_1 = require("../MainProcess/Store");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var Window_Main = /** @class */ (function () {
    function Window_Main(params) {
        var _self = this;
        var cfg = Store_1.MainProcess_Store.getInstance().get('window');
        this._window = new electron_1.BrowserWindow(this._getWindowOptions(cfg));
        this._parent = params.parent;
        this._window.on('close', function (event) {
            if (!_self._parent.willQuitApp()) {
                event.preventDefault();
                _self._window.hide();
            }
        });
        this._window.on('closed', function () {
            // Сброс объекта окна, обычно нужен когда вы храните окна
            // в массиве, это нужно если в вашем приложении множество окон, 
            // в таком случае вы должны удалить соответствующий элемент.
            //        let config = {
            //            size: mainWindow.getSize(),
            //            position: mainWindow.getPosition(),
            //        }
            //
            //        fs.writeFile("/tmp/test", JSON.stringify(config), function (err)
            //        {
            //            if (err) {
            //                return console.log(err);
            //            }
            //
            //            console.log("The file was saved!");
            //        });
            _self._window = null;
        });
        this._window.loadURL(cfg.url);
        this._window.webContents.on('dom-ready', function () {
            _self._window.webContents.executeJavaScript('binder.bind()');
        });
    }
    /**
     *
     * @returns {Window_Main}
     */
    Window_Main.getInstance = function () {
        return Window_Main._self ? Window_Main._self : (Window_Main._self = new Window_Main({ parent: App_1.MainProcess_App.getInstance() }));
    };
    Window_Main.prototype.getBrowserWindow = function () {
        return this._window;
    };
    Window_Main.prototype.getSettings = function () {
        var position = this._window.getPosition();
        var data = {
            url: this._window.webContents.getURL(),
            x: position[0],
            y: position[1]
        };
        if (!this._parent.isMini()) {
            var size = this._window.getSize();
            data['width'] = size[0];
            data['height'] = size[1];
        }
        return data;
    };
    Window_Main.prototype.mini = function () {
        var screenSize = electron_1.screen.getPrimaryDisplay().size;
        this._window.setContentSize(460, 125);
        //        this._window.setPosition(screenSize.width - 460, 10);
    };
    Window_Main.prototype._getWindowOptions = function (cfg) {
        var data = {
            width: cfg.width,
            height: cfg.height,
            webPreferences: {
                nodeIntegration: false,
                contextIsolation: false,
                preload: path.join(global['APPLICATION_PATH'], 'App', 'preload.js')
            }
        };
        if (cfg.x !== null && cfg.y !== null) {
            data.x = cfg.x;
            data.y = cfg.y;
        }
        return data;
    };
    return Window_Main;
}());
exports.Window_Main = Window_Main;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWFpbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9XaW5kb3cvTWFpbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHFDQUFnRjtBQUNoRiwwQ0FBbUQ7QUFDbkQsMkJBQThCO0FBQzlCLDhDQUF1RDtBQVN2RDs7OztHQUlHO0FBQ0g7SUF5Q0kscUJBQXNCLE1BQWlDO1FBQ25ELElBQUksS0FBSyxHQUFHLElBQUksQ0FBQztRQUNqQixJQUFJLEdBQUcsR0FBc0IseUJBQWlCLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRTNFLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSx3QkFBYSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUU3QixJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxLQUFZO1lBQzNDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQy9CLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDdkIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUN6QixDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUU7WUFDdEIseURBQXlEO1lBQ3pELGdFQUFnRTtZQUNoRSw0REFBNEQ7WUFDNUQsd0JBQXdCO1lBQ3hCLHlDQUF5QztZQUN6QyxpREFBaUQ7WUFDakQsV0FBVztZQUNYLEVBQUU7WUFDRiwwRUFBMEU7WUFDMUUsV0FBVztZQUNYLHdCQUF3QjtZQUN4QiwwQ0FBMEM7WUFDMUMsZUFBZTtZQUNmLEVBQUU7WUFDRixpREFBaUQ7WUFDakQsYUFBYTtZQUViLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxXQUFXLEVBQUU7WUFDckMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDakUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBMUVEOzs7T0FHRztJQUNXLHVCQUFXLEdBQXpCO1FBQ0ksTUFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFDLE1BQU0sRUFBRSxxQkFBZSxDQUFDLFdBQVcsRUFBRSxFQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2xJLENBQUM7SUFFTSxzQ0FBZ0IsR0FBdkI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN4QixDQUFDO0lBRU0saUNBQVcsR0FBbEI7UUFDSSxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQzFDLElBQUksSUFBSSxHQUFHO1lBQ1AsR0FBRyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRTtZQUN0QyxDQUFDLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUNkLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDO1NBRWpCLENBQUM7UUFDRixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDbEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdCLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSwwQkFBSSxHQUFYO1FBQ0ksSUFBSSxVQUFVLEdBQUcsaUJBQU0sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLElBQUksQ0FBQztRQUNqRCxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDOUMsK0RBQStEO0lBQzNELENBQUM7SUE0Q1MsdUNBQWlCLEdBQTNCLFVBQTRCLEdBQXNCO1FBQzlDLElBQUksSUFBSSxHQUFvQztZQUN4QyxLQUFLLEVBQUUsR0FBRyxDQUFDLEtBQUs7WUFDaEIsTUFBTSxFQUFFLEdBQUcsQ0FBQyxNQUFNO1lBQ2xCLGNBQWMsRUFBRTtnQkFDWixlQUFlLEVBQUUsS0FBSztnQkFDdEIsZ0JBQWdCLEVBQUUsS0FBSztnQkFDdkIsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGtCQUFrQixDQUFDLEVBQUUsS0FBSyxFQUFFLFlBQVksQ0FBQzthQUN0RTtTQUNKLENBQUM7UUFFRixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ2YsSUFBSSxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ25CLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFDTCxrQkFBQztBQUFELENBQUMsQUFuR0QsSUFtR0M7QUFuR1ksa0NBQVcifQ==