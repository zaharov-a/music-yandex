"use strict";
exports.__esModule = true;
var electron_1 = require("electron");
var path = require("path");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var MainProcess_Tray_Default = /** @class */ (function () {
    function MainProcess_Tray_Default(params) {
        this._clicked = 0;
        this._items = {};
        var _self = this;
        this._parent = params.parent;
        this._tray = new electron_1.Tray(this._getIcon());
        this._contextMenu = new electron_1.Menu();
        this._appendMenuItems();
        this._bind();
        this._tray.setContextMenu(this._contextMenu);
    }
    MainProcess_Tray_Default.prototype.setImage = function (image) {
        this._tray.setImage(path.join(global['APPLICATION_PATH'], 'Resources', 'tray', image + ".png"));
    };
    MainProcess_Tray_Default.prototype.setToolTip = function (title) {
        this._tray.setToolTip(title);
    };
    MainProcess_Tray_Default.prototype.setPlayingState = function (state) {
        this._items['pause'].checked = !state;
        if (this._parent.isLinux()) {
            this._tray.setContextMenu(this._contextMenu);
        }
    };
    MainProcess_Tray_Default.prototype.setShuffleState = function (state) {
        this._shuffle = state;
        this._items['shuffle'].checked = state;
        if (this._parent.isLinux()) {
            this._tray.setContextMenu(this._contextMenu);
        }
    };
    MainProcess_Tray_Default.prototype.setRepeatState = function (state) {
        var submenu = this._repeatMenu;
        for (var _i = 0, _a = submenu.items; _i < _a.length; _i++) {
            var item = _a[_i];
            item.checked = false;
        }
        this._repeat = 'false';
        switch (true) {
            case (state === true):
                this._repeat = 'true';
                submenu.items[1].checked = true;
                break;
            case (state === 1): {
                this._repeat = '1';
                submenu.items[2].checked = true;
                break;
            }
            default:
                submenu.items[0].checked = true;
                break;
        }
        if (this._parent.isLinux()) {
            this._tray.setContextMenu(this._contextMenu);
        }
    };
    MainProcess_Tray_Default.prototype.setMini = function (state) {
        this._items['mini'].checked = state;
        if (this._parent.isLinux()) {
            this._tray.setContextMenu(this._contextMenu);
        }
        if (this._items['mini'].checked) {
            this._parent.getMainWindow().mini();
        }
    };
    MainProcess_Tray_Default.prototype.isChecked = function (name) {
        return this._items[name].checked;
    };
    MainProcess_Tray_Default.prototype._getIcon = function () {
        return path.join(global['APPLICATION_PATH'], 'Resources', 'tray', 'pause.png');
    };
    MainProcess_Tray_Default.prototype._bind = function () {
        var _self = this;
        this._tray.on('click', function () {
            _self._clicked++;
            if (_self._clickTimeout) {
                clearTimeout(_self._clickTimeout);
            }
            _self._clickTimeout = setTimeout(function () {
                _self._onClick();
            }, 300);
        });
    };
    MainProcess_Tray_Default.prototype._onClick = function () {
        switch (this._clicked) {
            case 4:
                this._parent.getExternalAPI().prev();
                break;
            case 3:
                this._parent.getExternalAPI().next();
                break;
            default:
                this._parent.getExternalAPI().togglePause();
                break;
        }
        this._clicked = 0;
    };
    MainProcess_Tray_Default.prototype._appendMenuItems = function () {
        this._appendMenuItemNext();
        this._appendMenuItemPrev();
        this._appendMenuItemPause();
        this._appendMenuItemMini();
        this._appendSeparator();
        //this._appendMenuItemPlay();
        this._appendMenuItemShuffle();
        this._appendMenuItemRepeat();
        this._appendMenuItemShow();
        this._appendSeparator();
        this._appendMenuItemQuit();
    };
    MainProcess_Tray_Default.prototype._appendSeparator = function () {
        this._contextMenu.append(new electron_1.MenuItem({ type: 'separator' }));
    };
    MainProcess_Tray_Default.prototype._appendMenuItemNext = function () {
        var _self = this;
        var item = new electron_1.MenuItem({
            label: 'Далее',
            type: 'normal',
            click: function (menuItem, browserWindow, event) {
                _self._parent.getExternalAPI().next();
            }
        });
        this._items['next'] = item;
        this._contextMenu.append(item);
    };
    MainProcess_Tray_Default.prototype._appendMenuItemPrev = function () {
        var _self = this;
        var item = new electron_1.MenuItem({
            label: 'Назад',
            type: 'normal',
            click: function (menuItem, browserWindow, event) {
                _self._parent.getExternalAPI().prev();
            }
        });
        this._items['prev'] = item;
        this._contextMenu.append(item);
    };
    MainProcess_Tray_Default.prototype._appendMenuItemPause = function () {
        var _self = this;
        var item = new electron_1.MenuItem({
            label: 'Пауза',
            type: 'checkbox',
            checked: true,
            click: function (menuItem, browserWindow, event) {
                _self._parent.getExternalAPI().togglePause();
            }
        });
        this._items['pause'] = item;
        this._contextMenu.append(item);
    };
    MainProcess_Tray_Default.prototype._appendMenuItemPlay = function () {
        var _self = this;
        var item = new electron_1.MenuItem({
            label: 'Play',
            type: 'normal',
            click: function (menuItem, browserWindow, event) {
                _self._parent.getExternalAPI().play();
            }
        });
        this._items['play'] = item;
        this._contextMenu.append(item);
    };
    MainProcess_Tray_Default.prototype._appendMenuItemShow = function () {
        var _self = this;
        var item = new electron_1.MenuItem({
            label: 'Показать',
            type: 'normal',
            click: function (menuItem, browserWindow, event) {
                _self._parent.getMainWindow().getBrowserWindow().show();
                if (_self._items['mini'].checked) {
                    _self._parent.getMainWindow().mini();
                }
            }
        });
        this._items['play'] = item;
        this._contextMenu.append(item);
    };
    MainProcess_Tray_Default.prototype._appendMenuItemQuit = function () {
        var _self = this;
        var item = new electron_1.MenuItem({
            label: 'Завершить',
            type: 'normal',
            click: function (menuItem, browserWindow, event) {
                _self._parent.quit();
            }
        });
        this._items['quit'] = item;
        this._contextMenu.append(item);
    };
    MainProcess_Tray_Default.prototype._appendMenuItemShuffle = function () {
        var _self = this;
        var item = new electron_1.MenuItem({
            label: 'Перемешивание',
            type: 'checkbox',
            checked: false,
            click: function (menuItem, browserWindow, event) {
                _self._parent.getExternalAPI().toggleShuffle();
            }
        });
        this._items['shuffle'] = item;
        this._contextMenu.append(item);
    };
    MainProcess_Tray_Default.prototype._appendMenuItemRepeat = function () {
        var _self = this;
        var menu = electron_1.Menu.buildFromTemplate([
            {
                label: 'Выкл.',
                type: 'checkbox',
                checked: true,
                click: function () {
                    _self._parent.getExternalAPI().toggleRepeat('false');
                }
            },
            {
                label: 'Всех',
                type: 'checkbox',
                click: function () {
                    _self._parent.getExternalAPI().toggleRepeat('true');
                }
            },
            {
                label: 'Одной',
                type: 'checkbox',
                click: function () {
                    _self._parent.getExternalAPI().toggleRepeat('1');
                }
            },
        ]);
        var item = new electron_1.MenuItem({
            label: 'Повтор',
            submenu: menu
        });
        this._items['repeat'] = item;
        this._repeatMenu = menu;
        this._contextMenu.append(item);
    };
    MainProcess_Tray_Default.prototype._appendMenuItemMini = function () {
        var _self = this;
        var item = new electron_1.MenuItem({
            label: 'Мини-плеер',
            type: 'checkbox',
            click: function (menuItem, browserWindow, event) {
                var window = _self._parent.getMainWindow();
                window.getBrowserWindow().setAlwaysOnTop(menuItem.checked);
                if (menuItem.checked) {
                    window.mini();
                }
            }
        });
        this._items['mini'] = item;
        this._contextMenu.append(item);
    };
    return MainProcess_Tray_Default;
}());
exports.MainProcess_Tray_Default = MainProcess_Tray_Default;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGVmYXVsdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9NYWluUHJvY2Vzcy9UcmF5L0RlZmF1bHQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSxxQ0FBcUU7QUFDckUsMkJBQThCO0FBRzlCOzs7O0dBSUc7QUFDSDtJQVlJLGtDQUFtQixNQUFpQztRQVAxQyxhQUFRLEdBQUcsQ0FBQyxDQUFDO1FBRWIsV0FBTSxHQUFtQyxFQUFFLENBQUM7UUFNbEQsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUM3QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksZUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxlQUFJLEVBQUUsQ0FBQztRQUUvQixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUV4QixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFFYixJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUlNLDJDQUFRLEdBQWYsVUFBZ0IsS0FBYTtRQUN6QixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUssS0FBSyxTQUFNLENBQUMsQ0FBQyxDQUFDO0lBQ3BHLENBQUM7SUFHTSw2Q0FBVSxHQUFqQixVQUFrQixLQUFhO1FBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFTSxrREFBZSxHQUF0QixVQUF1QixLQUFjO1FBQ2pDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxHQUFHLENBQUMsS0FBSyxDQUFDO1FBRXRDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNqRCxDQUFDO0lBQ0wsQ0FBQztJQUVNLGtEQUFlLEdBQXRCLFVBQXVCLEtBQWM7UUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBRXZDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNqRCxDQUFDO0lBQ0wsQ0FBQztJQUVNLGlEQUFjLEdBQXJCLFVBQXNCLEtBQXVCO1FBQ3pDLElBQUksT0FBTyxHQUFTLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDckMsR0FBRyxDQUFDLENBQWEsVUFBYSxFQUFiLEtBQUEsT0FBTyxDQUFDLEtBQUssRUFBYixjQUFhLEVBQWIsSUFBYTtZQUF6QixJQUFJLElBQUksU0FBQTtZQUNULElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQ3hCO1FBQ0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFFdkIsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNYLEtBQUssQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDO2dCQUNqQixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztnQkFDdEIsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO2dCQUNoQyxLQUFLLENBQUM7WUFDVixLQUFLLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ2pCLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO2dCQUNuQixPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7Z0JBQ2hDLEtBQUssQ0FBQztZQUNWLENBQUM7WUFDRDtnQkFDSSxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7Z0JBQ2hDLEtBQUssQ0FBQztRQUNkLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQztZQUN6QixJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDakQsQ0FBQztJQUNMLENBQUM7SUFFTSwwQ0FBTyxHQUFkLFVBQWUsS0FBYztRQUN6QixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFFcEMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2pELENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN4QyxDQUFDO0lBQ0wsQ0FBQztJQUVNLDRDQUFTLEdBQWhCLFVBQWlCLElBQVk7UUFDekIsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDO0lBQ3JDLENBQUM7SUFFUywyQ0FBUSxHQUFsQjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsV0FBVyxDQUFDLENBQUM7SUFDbkYsQ0FBQztJQUVTLHdDQUFLLEdBQWY7UUFDSSxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFO1lBQ25CLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUVqQixFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztnQkFDdEIsWUFBWSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN0QyxDQUFDO1lBQ0QsS0FBSyxDQUFDLGFBQWEsR0FBRyxVQUFVLENBQUM7Z0JBQzdCLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNyQixDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFUywyQ0FBUSxHQUFsQjtRQUNJLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLEtBQUssQ0FBQztnQkFDRixJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUNyQyxLQUFLLENBQUM7WUFDVixLQUFLLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDckMsS0FBSyxDQUFDO1lBQ1Y7Z0JBQ0ksSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDNUMsS0FBSyxDQUFDO1FBQ2QsQ0FBQztRQUNELElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUFFUyxtREFBZ0IsR0FBMUI7UUFDSSxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUN4Qiw2QkFBNkI7UUFDN0IsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVTLG1EQUFnQixHQUExQjtRQUNJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLElBQUksbUJBQVEsQ0FBQyxFQUFDLElBQUksRUFBRSxXQUFXLEVBQUMsQ0FBQyxDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUVTLHNEQUFtQixHQUE3QjtRQUNJLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQztRQUNqQixJQUFJLElBQUksR0FBRyxJQUFJLG1CQUFRLENBQUM7WUFDcEIsS0FBSyxFQUFFLE9BQU87WUFDZCxJQUFJLEVBQUUsUUFBUTtZQUNkLEtBQUssRUFBRSxVQUFDLFFBQWtCLEVBQUUsYUFBNEIsRUFBRSxLQUFZO2dCQUNsRSxLQUFLLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFDLENBQUM7U0FDSixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRVMsc0RBQW1CLEdBQTdCO1FBQ0ksSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksSUFBSSxHQUFHLElBQUksbUJBQVEsQ0FBQztZQUNwQixLQUFLLEVBQUUsT0FBTztZQUNkLElBQUksRUFBRSxRQUFRO1lBQ2QsS0FBSyxFQUFFLFVBQUMsUUFBa0IsRUFBRSxhQUE0QixFQUFFLEtBQVk7Z0JBQ2xFLEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUMsQ0FBQztTQUNKLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBQzNCLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFUyx1REFBb0IsR0FBOUI7UUFDSSxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxJQUFJLEdBQUcsSUFBSSxtQkFBUSxDQUFDO1lBQ3BCLEtBQUssRUFBRSxPQUFPO1lBQ2QsSUFBSSxFQUFFLFVBQVU7WUFDaEIsT0FBTyxFQUFFLElBQUk7WUFDYixLQUFLLEVBQUUsVUFBQyxRQUFrQixFQUFFLGFBQTRCLEVBQUUsS0FBWTtnQkFDbEUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNqRCxDQUFDO1NBQ0osQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUM7UUFDNUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVTLHNEQUFtQixHQUE3QjtRQUNJLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQztRQUNqQixJQUFJLElBQUksR0FBRyxJQUFJLG1CQUFRLENBQUM7WUFDcEIsS0FBSyxFQUFFLE1BQU07WUFDYixJQUFJLEVBQUUsUUFBUTtZQUNkLEtBQUssRUFBRSxVQUFDLFFBQWtCLEVBQUUsYUFBNEIsRUFBRSxLQUFZO2dCQUNsRSxLQUFLLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFDLENBQUM7U0FDSixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRVMsc0RBQW1CLEdBQTdCO1FBQ0ksSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksSUFBSSxHQUFHLElBQUksbUJBQVEsQ0FBQztZQUNwQixLQUFLLEVBQUUsVUFBVTtZQUNqQixJQUFJLEVBQUUsUUFBUTtZQUNkLEtBQUssRUFBRSxVQUFDLFFBQWtCLEVBQUUsYUFBNEIsRUFBRSxLQUFZO2dCQUNsRSxLQUFLLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxDQUFDLGdCQUFnQixFQUFFLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ3hELEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztvQkFDL0IsS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDekMsQ0FBQztZQUNMLENBQUM7U0FDSixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRVMsc0RBQW1CLEdBQTdCO1FBQ0ksSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksSUFBSSxHQUFHLElBQUksbUJBQVEsQ0FBQztZQUNwQixLQUFLLEVBQUUsV0FBVztZQUNsQixJQUFJLEVBQUUsUUFBUTtZQUNkLEtBQUssRUFBRSxVQUFDLFFBQWtCLEVBQUUsYUFBNEIsRUFBRSxLQUFZO2dCQUNsRSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3pCLENBQUM7U0FDSixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRVMseURBQXNCLEdBQWhDO1FBQ0ksSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksSUFBSSxHQUFHLElBQUksbUJBQVEsQ0FBQztZQUNwQixLQUFLLEVBQUUsZUFBZTtZQUN0QixJQUFJLEVBQUUsVUFBVTtZQUNoQixPQUFPLEVBQUUsS0FBSztZQUNkLEtBQUssRUFBRSxVQUFDLFFBQWtCLEVBQUUsYUFBNEIsRUFBRSxLQUFZO2dCQUNsRSxLQUFLLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ25ELENBQUM7U0FDSixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQztRQUM5QixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRVMsd0RBQXFCLEdBQS9CO1FBQ0ksSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksSUFBSSxHQUFHLGVBQUksQ0FBQyxpQkFBaUIsQ0FBQztZQUM5QjtnQkFDSSxLQUFLLEVBQUUsT0FBTztnQkFDZCxJQUFJLEVBQUUsVUFBVTtnQkFDaEIsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsS0FBSyxFQUFFO29CQUNILEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUN6RCxDQUFDO2FBQ0o7WUFDRDtnQkFDSSxLQUFLLEVBQUUsTUFBTTtnQkFDYixJQUFJLEVBQUUsVUFBVTtnQkFDaEIsS0FBSyxFQUFFO29CQUNILEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN4RCxDQUFDO2FBQ0o7WUFDRDtnQkFDSSxLQUFLLEVBQUUsT0FBTztnQkFDZCxJQUFJLEVBQUUsVUFBVTtnQkFDaEIsS0FBSyxFQUFFO29CQUNILEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNyRCxDQUFDO2FBQ0o7U0FDSixDQUFDLENBQUM7UUFDSCxJQUFJLElBQUksR0FBRyxJQUFJLG1CQUFRLENBQUM7WUFDcEIsS0FBSyxFQUFFLFFBQVE7WUFDZixPQUFPLEVBQUUsSUFBSTtTQUNoQixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRU0sc0RBQW1CLEdBQTFCO1FBQ0ksSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksSUFBSSxHQUFHLElBQUksbUJBQVEsQ0FBQztZQUNwQixLQUFLLEVBQUUsWUFBWTtZQUNuQixJQUFJLEVBQUUsVUFBVTtZQUNoQixLQUFLLEVBQUUsVUFBQyxRQUFrQixFQUFFLGFBQTRCLEVBQUUsS0FBWTtnQkFDbEUsSUFBSSxNQUFNLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFFM0MsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDM0QsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ25CLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDbEIsQ0FBQztZQUdMLENBQUM7U0FDSixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBQ0wsK0JBQUM7QUFBRCxDQUFDLEFBbFRELElBa1RDO0FBbFRZLDREQUF3QiJ9