"use strict";
exports.__esModule = true;
var App_1 = require("../App");
var Default_1 = require("./Default");
var OSX_1 = require("./OSX");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var MainProcess_Tray_Factory = /** @class */ (function () {
    function MainProcess_Tray_Factory() {
    }
    MainProcess_Tray_Factory.getInstance = function () {
        return MainProcess_Tray_Factory._tray
            ? MainProcess_Tray_Factory._tray
            : MainProcess_Tray_Factory._create();
    };
    MainProcess_Tray_Factory._create = function () {
        var app = App_1.MainProcess_App.getInstance();
        var params = { parent: app };
        switch (true) {
            case app.isOSX():
                return new OSX_1.MainProcess_Tray_OSX(params);
            default:
                return new Default_1.MainProcess_Tray_Default(params);
        }
    };
    return MainProcess_Tray_Factory;
}());
exports.MainProcess_Tray_Factory = MainProcess_Tray_Factory;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRmFjdG9yeS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9NYWluUHJvY2Vzcy9UcmF5L0ZhY3RvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw4QkFBdUM7QUFDdkMscUNBQW1EO0FBQ25ELDZCQUEyQztBQUMzQzs7OztHQUlHO0FBQ0g7SUFBQTtJQWtCQSxDQUFDO0lBaEJpQixvQ0FBVyxHQUF6QjtRQUNJLE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLO1lBQ2pDLENBQUMsQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLO1lBQ2hDLENBQUMsQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUM3QyxDQUFDO0lBRWdCLGdDQUFPLEdBQXhCO1FBQ0ksSUFBSSxHQUFHLEdBQUcscUJBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUN4QyxJQUFJLE1BQU0sR0FBRyxFQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUMsQ0FBQztRQUMzQixNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ1gsS0FBSyxHQUFHLENBQUMsS0FBSyxFQUFFO2dCQUNaLE1BQU0sQ0FBQyxJQUFJLDBCQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVDO2dCQUNJLE1BQU0sQ0FBQyxJQUFJLGtDQUF3QixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3BELENBQUM7SUFDTCxDQUFDO0lBQ0wsK0JBQUM7QUFBRCxDQUFDLEFBbEJELElBa0JDO0FBbEJZLDREQUF3QiJ9