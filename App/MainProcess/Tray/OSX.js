"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Default_1 = require("./Default");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var MainProcess_Tray_OSX = /** @class */ (function (_super) {
    __extends(MainProcess_Tray_OSX, _super);
    function MainProcess_Tray_OSX() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MainProcess_Tray_OSX.prototype._bind = function () {
        var _self = this;
        this._tray.on('right-click', function () {
            _self._onClick();
        });
    };
    return MainProcess_Tray_OSX;
}(Default_1.MainProcess_Tray_Default));
exports.MainProcess_Tray_OSX = MainProcess_Tray_OSX;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiT1NYLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL01haW5Qcm9jZXNzL1RyYXkvT1NYLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLHFDQUFtRDtBQUNuRDs7OztHQUlHO0FBQ0g7SUFBMEMsd0NBQXdCO0lBQWxFOztJQU9BLENBQUM7SUFOYSxvQ0FBSyxHQUFmO1FBQ0ksSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRTtZQUN6QixLQUFLLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0wsMkJBQUM7QUFBRCxDQUFDLEFBUEQsQ0FBMEMsa0NBQXdCLEdBT2pFO0FBUFksb0RBQW9CIn0=