import {MainProcess_App} from '../App';
import {Menu, Tray, MenuItem, BrowserWindow, screen} from 'electron';
import path = require('path');

import {IExternalAPI_TrackInfo} from '../../ExternalAPI/interfaces';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
export class MainProcess_Tray_Default {
    protected _parent: MainProcess_App;
    protected _tray: Tray;
    protected _contextMenu: Menu;
    protected _repeatMenu: Menu;
    protected _clicked = 0;
    protected _clickTimeout: NodeJS.Timer;
    protected _items: {[property: string]: MenuItem} = {};
    //controls
    protected _shuffle: boolean;
    protected _repeat: string;

    public constructor(params: {parent: MainProcess_App}) {
        let _self = this;
        this._parent = params.parent;
        this._tray = new Tray(this._getIcon());
        this._contextMenu = new Menu();

        this._appendMenuItems();

        this._bind();

        this._tray.setContextMenu(this._contextMenu);
    }



    public setImage(image: string) {
        this._tray.setImage(path.join(global['APPLICATION_PATH'], 'Resources', 'tray', `${image}.png`));
    }


    public setToolTip(title: string) {
        this._tray.setToolTip(title);
    }

    public setPlayingState(state: boolean) {
        this._items['pause'].checked = !state;

        if (this._parent.isLinux()) {
            this._tray.setContextMenu(this._contextMenu);
        }
    }

    public setShuffleState(state: boolean) {
        this._shuffle = state;
        this._items['shuffle'].checked = state;

        if (this._parent.isLinux()) {
            this._tray.setContextMenu(this._contextMenu);
        }
    }

    public setRepeatState(state: boolean | number) {
        let submenu: Menu = this._repeatMenu;
        for (let item of submenu.items) {
            item.checked = false;
        }
        this._repeat = 'false';

        switch (true) {
            case (state === true):
                this._repeat = 'true';
                submenu.items[1].checked = true;
                break;
            case (state === 1): {
                this._repeat = '1';
                submenu.items[2].checked = true;
                break;
            }
            default:
                submenu.items[0].checked = true;
                break;
        }

        if (this._parent.isLinux()) {
            this._tray.setContextMenu(this._contextMenu);
        }
    }

    public setMini(state: boolean) {
        this._items['mini'].checked = state;

        if (this._parent.isLinux()) {
            this._tray.setContextMenu(this._contextMenu);
        }

        if (this._items['mini'].checked) {
            this._parent.getMainWindow().mini();
        }
    }

    public isChecked(name: string) {
        return this._items[name].checked;
    }

    protected _getIcon() {
        return path.join(global['APPLICATION_PATH'], 'Resources', 'tray', 'pause.png');
    }

    protected _bind() {
        let _self = this;
        this._tray.on('click', () => {
            _self._clicked++;

            if (_self._clickTimeout) {
                clearTimeout(_self._clickTimeout);
            }
            _self._clickTimeout = setTimeout(function () {
                _self._onClick();
            }, 300);
        });
    }

    protected _onClick() {
        switch (this._clicked) {
            case 4:
                this._parent.getExternalAPI().prev();
                break;
            case 3:
                this._parent.getExternalAPI().next();
                break;
            default:
                this._parent.getExternalAPI().togglePause();
                break;
        }
        this._clicked = 0;
    }

    protected _appendMenuItems() {
        this._appendMenuItemNext();
        this._appendMenuItemPrev();
        this._appendMenuItemPause();
        this._appendMenuItemMini();
        this._appendSeparator();
        //this._appendMenuItemPlay();
        this._appendMenuItemShuffle();
        this._appendMenuItemRepeat();
        this._appendMenuItemShow();
        this._appendSeparator();
        this._appendMenuItemQuit();
    }

    protected _appendSeparator() {
        this._contextMenu.append(new MenuItem({type: 'separator'}));
    }

    protected _appendMenuItemNext() {
        let _self = this;
        let item = new MenuItem({
            label: 'Далее',//'next',
            type: 'normal',
            click: (menuItem: MenuItem, browserWindow: BrowserWindow, event: Event) => {
                _self._parent.getExternalAPI().next();
            }
        });

        this._items['next'] = item;
        this._contextMenu.append(item);
    }

    protected _appendMenuItemPrev() {
        let _self = this;
        let item = new MenuItem({
            label: 'Назад',//'prev',
            type: 'normal',
            click: (menuItem: MenuItem, browserWindow: BrowserWindow, event: Event) => {
                _self._parent.getExternalAPI().prev();
            }
        });

        this._items['prev'] = item;
        this._contextMenu.append(item);
    }

    protected _appendMenuItemPause() {
        let _self = this;
        let item = new MenuItem({
            label: 'Пауза',//'pause',
            type: 'checkbox',
            checked: true,
            click: (menuItem: MenuItem, browserWindow: BrowserWindow, event: Event) => {
                _self._parent.getExternalAPI().togglePause();
            }
        });

        this._items['pause'] = item;
        this._contextMenu.append(item);
    }

    protected _appendMenuItemPlay() {
        let _self = this;
        let item = new MenuItem({
            label: 'Play',//'play',
            type: 'normal',
            click: (menuItem: MenuItem, browserWindow: BrowserWindow, event: Event) => {
                _self._parent.getExternalAPI().play();
            }
        });

        this._items['play'] = item;
        this._contextMenu.append(item);
    }

    protected _appendMenuItemShow() {
        let _self = this;
        let item = new MenuItem({
            label: 'Показать',//'play',
            type: 'normal',
            click: (menuItem: MenuItem, browserWindow: BrowserWindow, event: Event) => {
                _self._parent.getMainWindow().getBrowserWindow().show();
                if (_self._items['mini'].checked) {
                    _self._parent.getMainWindow().mini();
                }
            }
        });

        this._items['play'] = item;
        this._contextMenu.append(item);
    }

    protected _appendMenuItemQuit() {
        let _self = this;
        let item = new MenuItem({
            label: 'Завершить',//'play',
            type: 'normal',
            click: (menuItem: MenuItem, browserWindow: BrowserWindow, event: Event) => {
                _self._parent.quit();
            }
        });

        this._items['quit'] = item;
        this._contextMenu.append(item);
    }

    protected _appendMenuItemShuffle() {
        let _self = this;
        let item = new MenuItem({
            label: 'Перемешивание',//'shuffle',
            type: 'checkbox',
            checked: false,
            click: (menuItem: MenuItem, browserWindow: BrowserWindow, event: Event) => {
                _self._parent.getExternalAPI().toggleShuffle();
            }
        });

        this._items['shuffle'] = item;
        this._contextMenu.append(item);
    }

    protected _appendMenuItemRepeat() {
        let _self = this;
        let menu = Menu.buildFromTemplate([
            {
                label: 'Выкл.',
                type: 'checkbox',
                checked: true,
                click: () => {
                    _self._parent.getExternalAPI().toggleRepeat('false');
                }
            },
            {
                label: 'Всех',
                type: 'checkbox',
                click: () => {
                    _self._parent.getExternalAPI().toggleRepeat('true');
                }
            },
            {
                label: 'Одной',
                type: 'checkbox',
                click: () => {
                    _self._parent.getExternalAPI().toggleRepeat('1');
                }
            },
        ]);
        let item = new MenuItem({
            label: 'Повтор',//'repeat',
            submenu: menu,
        });

        this._items['repeat'] = item;
        this._repeatMenu = menu;
        this._contextMenu.append(item);
    }

    public _appendMenuItemMini() {
        let _self = this;
        let item = new MenuItem({
            label: 'Мини-плеер',
            type: 'checkbox',
            click: (menuItem: MenuItem, browserWindow: BrowserWindow, event: Event) => {
                let window = _self._parent.getMainWindow();

                window.getBrowserWindow().setAlwaysOnTop(menuItem.checked);
                if (menuItem.checked) {
                    window.mini();
                }


            }
        });

        this._items['mini'] = item;
        this._contextMenu.append(item);
    }
}