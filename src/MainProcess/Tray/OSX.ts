import {MainProcess_Tray_Default} from './Default';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
export class MainProcess_Tray_OSX extends MainProcess_Tray_Default {
    protected _bind() {
        let _self = this;
        this._tray.on('right-click', () => {
            _self._onClick();
        });
    }
}