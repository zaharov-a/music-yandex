"use strict";
exports.__esModule = true;
var electron_1 = require("electron");
var Renderer_1 = require("./ExternalAPI/Renderer");
//import {Binder} from 'binder';
//import Binder from 'node_modules/../../ts/binder';
process.once('loaded', function () {
    global['ipc'] = electron_1.ipcRenderer;
    global['binder'] = new Renderer_1.ExternalAPI_Renderer(electron_1.ipcRenderer);
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJlbG9hZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy9wcmVsb2FkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEscUNBQXFDO0FBQ3JDLG1EQUE4RDtBQUM5RCxnQ0FBZ0M7QUFDaEMsb0RBQW9EO0FBR3BELE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO0lBQ25CLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxzQkFBVyxDQUFDO0lBQzVCLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLCtCQUFvQixDQUFDLHNCQUFXLENBQUMsQ0FBQztBQUM3RCxDQUFDLENBQUMsQ0FBQyJ9