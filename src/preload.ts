import {ipcRenderer} from 'electron';
import { ExternalAPI_Renderer } from "./ExternalAPI/Renderer";
//import {Binder} from 'binder';
//import Binder from 'node_modules/../../ts/binder';


process.once('loaded', () => {
    global['ipc'] = ipcRenderer;
    global['binder'] = new ExternalAPI_Renderer(ipcRenderer);
});