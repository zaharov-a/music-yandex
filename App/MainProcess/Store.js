"use strict";
exports.__esModule = true;
var electron_1 = require("electron");
var fs = require("fs");
var path = require("path");
var MainProcess_Store = /** @class */ (function () {
    function MainProcess_Store() {
        this._data = {
            window: {
                width: 900,
                height: 600,
                url: 'https://music.yandex.ru/artist/1529749',
                display: 0,
                x: null,
                y: null
            },
            mini: false
        };
        this._userDataPath = path.join((electron_1.app || electron_1.remote.app).getPath('userData'), 'config.json');
        this._parseDataFile();
    }
    MainProcess_Store.getInstance = function () {
        return MainProcess_Store._self ? MainProcess_Store._self : (MainProcess_Store._self = new MainProcess_Store());
    };
    MainProcess_Store.prototype.setProperties = function (data) {
        this._merge(this._data, data);
        return this;
    };
    MainProcess_Store.prototype.getProperties = function () {
        return this._data;
    };
    MainProcess_Store.prototype.get = function (name) {
        return this._data[name];
    };
    MainProcess_Store.prototype.save = function () {
        fs.writeFileSync(this._userDataPath, JSON.stringify(this._data));
        return this;
    };
    MainProcess_Store.prototype._parseDataFile = function () {
        try {
            var data = JSON.parse((fs.readFileSync(this._userDataPath)).toString());
            this.setProperties(data);
        }
        catch (error) {
        }
    };
    MainProcess_Store.prototype._merge = function (a, b) {
        for (var key in b) {
            if (a[key] === undefined) {
                continue;
            }
            if (typeof a[key] === 'object' && a[key] !== null) {
                this._merge(a[key], b[key]);
            }
            else {
                a[key] = b[key];
            }
        }
    };
    return MainProcess_Store;
}());
exports.MainProcess_Store = MainProcess_Store;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3RvcmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvTWFpblByb2Nlc3MvU3RvcmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxxQ0FBcUM7QUFDckMsdUJBQTBCO0FBQzFCLDJCQUE4QjtBQWM5QjtJQXVDSTtRQXBDVSxVQUFLLEdBQTBCO1lBQ3JDLE1BQU0sRUFBRTtnQkFDSixLQUFLLEVBQUUsR0FBRztnQkFDVixNQUFNLEVBQUUsR0FBRztnQkFDWCxHQUFHLEVBQUUsd0NBQXdDO2dCQUM3QyxPQUFPLEVBQUUsQ0FBQztnQkFDVixDQUFDLEVBQUUsSUFBSTtnQkFDUCxDQUFDLEVBQUUsSUFBSTthQUNWO1lBQ0QsSUFBSSxFQUFFLEtBQUs7U0FDZCxDQUFDO1FBMkJFLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLGNBQUcsSUFBSSxpQkFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsRUFBRSxhQUFhLENBQUMsQ0FBQztRQUN2RixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQTNCYSw2QkFBVyxHQUF6QjtRQUNJLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEdBQUcsSUFBSSxpQkFBaUIsRUFBRSxDQUFDLENBQUM7SUFDbkgsQ0FBQztJQUVNLHlDQUFhLEdBQXBCLFVBQXFCLElBQVE7UUFDekIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRTlCLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVNLHlDQUFhLEdBQXBCO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDdEIsQ0FBQztJQUVNLCtCQUFHLEdBQVYsVUFBVyxJQUFZO1FBQ25CLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFFTSxnQ0FBSSxHQUFYO1FBQ0ksRUFBRSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFFakUsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBT1MsMENBQWMsR0FBeEI7UUFDSSxJQUFJLENBQUM7WUFDRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQ3hFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDN0IsQ0FBQztRQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFFakIsQ0FBQztJQUNMLENBQUM7SUFFUyxrQ0FBTSxHQUFoQixVQUFpQixDQUE0QixFQUFFLENBQTRCO1FBQ3ZFLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDaEIsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLFFBQVEsQ0FBQztZQUNiLENBQUM7WUFFRCxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxRQUFRLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ2hELElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BCLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVMLHdCQUFDO0FBQUQsQ0FBQyxBQW5FRCxJQW1FQztBQW5FWSw4Q0FBaUIifQ==