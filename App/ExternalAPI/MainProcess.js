"use strict";
exports.__esModule = true;
var App_1 = require("../MainProcess/App");
var ExternalAPI_MainProcess = /** @class */ (function () {
    function ExternalAPI_MainProcess(params) {
        this._window = params.window;
    }
    ExternalAPI_MainProcess.getIstance = function () {
        return new ExternalAPI_MainProcess({ window: App_1.MainProcess_App.getInstance().getMainWindow().getBrowserWindow() });
    };
    ExternalAPI_MainProcess.prototype.play = function () {
        this._execute('play()');
    };
    ExternalAPI_MainProcess.prototype.next = function () {
        this._execute('next()');
    };
    ExternalAPI_MainProcess.prototype.prev = function () {
        this._execute('prev()');
    };
    ExternalAPI_MainProcess.prototype.togglePause = function () {
        this._execute('togglePause()');
    };
    ExternalAPI_MainProcess.prototype.toggleShuffle = function () {
        this._execute('toggleShuffle()');
    };
    ExternalAPI_MainProcess.prototype.toggleMute = function () {
        this._execute('toggleMute()');
    };
    ExternalAPI_MainProcess.prototype.toggleRepeat = function (state) {
        this._execute("toggleRepeat(" + state + ")");
    };
    ExternalAPI_MainProcess.prototype._execute = function (action) {
        this._window.webContents.executeJavaScript("externalAPI." + action);
    };
    return ExternalAPI_MainProcess;
}());
exports.ExternalAPI_MainProcess = ExternalAPI_MainProcess;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWFpblByb2Nlc3MuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvRXh0ZXJuYWxBUEkvTWFpblByb2Nlc3MudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSwwQ0FBbUQ7QUFFbkQ7SUFrQkksaUNBQW1CLE1BQStCO1FBQzlDLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNqQyxDQUFDO0lBTmEsa0NBQVUsR0FBeEI7UUFDSSxNQUFNLENBQUMsSUFBSSx1QkFBdUIsQ0FBQyxFQUFDLE1BQU0sRUFBRSxxQkFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDLGdCQUFnQixFQUFFLEVBQUMsQ0FBQyxDQUFDO0lBQ25ILENBQUM7SUFNTSxzQ0FBSSxHQUFYO1FBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRU0sc0NBQUksR0FBWDtRQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUVNLHNDQUFJLEdBQVg7UUFDSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFFTSw2Q0FBVyxHQUFsQjtRQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVNLCtDQUFhLEdBQXBCO1FBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFTSw0Q0FBVSxHQUFqQjtRQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVNLDhDQUFZLEdBQW5CLFVBQW9CLEtBQWE7UUFDN0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxrQkFBZ0IsS0FBSyxNQUFHLENBQUMsQ0FBQTtJQUMzQyxDQUFDO0lBRVMsMENBQVEsR0FBbEIsVUFBbUIsTUFBYztRQUM3QixJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBZSxNQUFRLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBQ0wsOEJBQUM7QUFBRCxDQUFDLEFBckRELElBcURDO0FBckRZLDBEQUF1QiJ9