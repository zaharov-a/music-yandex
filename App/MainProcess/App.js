"use strict";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
exports.__esModule = true;
var electron_1 = require("electron");
var path = require("path");
var Main_1 = require("../Window/Main");
var Factory_1 = require("./Tray/Factory");
var Store_1 = require("./Store");
var MainProcess_1 = require("../ExternalAPI/MainProcess");
var MainProcess_App = /** @class */ (function () {
    function MainProcess_App() {
        this._willQuitApp = false;
        this._isPlatform = {};
        electron_1.app.setName('YaM');
        electron_1.app.setPath('userData', path.join(electron_1.app.getPath('appData'), electron_1.app.getName()));
        this._settings = Store_1.MainProcess_Store.getInstance();
    }
    MainProcess_App.getInstance = function () {
        return MainProcess_App._self ? MainProcess_App._self : (MainProcess_App._self = new MainProcess_App());
    };
    MainProcess_App.prototype.getMainWindow = function () {
        return this._mainWindow;
    };
    MainProcess_App.prototype.getExternalAPI = function () {
        return this._binder;
    };
    MainProcess_App.prototype.isMini = function () {
        return this._tray.isChecked('mini');
    };
    MainProcess_App.prototype.run = function () {
        var _self = this;
        electron_1.app.on('ready', function () {
            _self._mainWindow = Main_1.Window_Main.getInstance();
            _self._binder = new MainProcess_1.ExternalAPI_MainProcess({ window: _self._mainWindow.getBrowserWindow() });
            _self._tray = Factory_1.MainProcess_Tray_Factory.getInstance();
            _self._tray.setMini(_self._settings.get('mini'));
            _self._initEvents();
        });
        electron_1.app.on('before-quit', function () {
            _self._willQuitApp = true;
            _self._settings.setProperties({
                window: _self._mainWindow.getSettings(),
                mini: _self.isMini()
            }).save();
        });
        //        app.on('window-all-closed', function () {
        //            // В OS X это характерно для приложений и их меню,
        //            // чтобы оставаться активными, пока пользователь явно не завершит работу 
        //            // при помощи Cmd + Q
        //            //if (process.platform != 'darwin') {
        //            app.quit();
        //            //}
        //        });
        this._isPlatform = {
            'linux': process.platform == 'linux',
            'windows': process.platform == 'win32',
            'osx': process.platform == 'darwin'
        };
    };
    MainProcess_App.prototype.quit = function () {
        this._willQuitApp = true;
        electron_1.app.quit();
    };
    MainProcess_App.prototype.willQuitApp = function () {
        return this._willQuitApp;
    };
    MainProcess_App.prototype.isOSX = function () {
        return this._isPlatform['osx'];
    };
    MainProcess_App.prototype.isLinux = function () {
        return this._isPlatform['linux'];
    };
    MainProcess_App.prototype.isWindows = function () {
        return this._isPlatform['windows'];
    };
    MainProcess_App.prototype._initWindow = function () {
        this._mainWindow = Main_1.Window_Main.getInstance();
    };
    MainProcess_App.prototype._initEvents = function () {
        var _self = this;
        electron_1.ipcMain.on('state', function (event, data) {
            _self._tray.setImage(data ? 'play' : 'pause');
            _self._tray.setPlayingState(data);
        });
        electron_1.ipcMain.on('volume', function (event, data) {
        });
        electron_1.ipcMain.on('track', function (event, data) {
            _self._tray.setToolTip(data.title);
        });
        electron_1.ipcMain.on('controls', function (event, data) {
            _self._tray.setShuffleState(data.shuffle);
            _self._tray.setRepeatState(data.repeat);
        });
        electron_1.ipcMain.on('progress', function (event, data) {
            _self._mainWindow.getBrowserWindow().setProgressBar(data || -1);
        });
    };
    return MainProcess_App;
}());
exports.MainProcess_App = MainProcess_App;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXBwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL01haW5Qcm9jZXNzL0FwcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7Ozs7R0FJRzs7QUFFSCxxQ0FBc0M7QUFDdEMsMkJBQThCO0FBQzlCLHVDQUEyQztBQUMzQywwQ0FBd0Q7QUFFeEQsaUNBQTBDO0FBQzFDLDBEQUFtRTtBQUduRTtJQVNJO1FBSlUsaUJBQVksR0FBRyxLQUFLLENBQUM7UUFDckIsZ0JBQVcsR0FBa0MsRUFBRSxDQUFDO1FBSXRELGNBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbkIsY0FBRyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFHLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxFQUFFLGNBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLFNBQVMsR0FBRyx5QkFBaUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNyRCxDQUFDO0lBRWEsMkJBQVcsR0FBekI7UUFDSSxNQUFNLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsS0FBSyxHQUFHLElBQUksZUFBZSxFQUFFLENBQUMsQ0FBQztJQUMzRyxDQUFDO0lBRU0sdUNBQWEsR0FBcEI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDO0lBRU0sd0NBQWMsR0FBckI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN4QixDQUFDO0lBRU0sZ0NBQU0sR0FBYjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN4QyxDQUFDO0lBRU0sNkJBQUcsR0FBVjtRQUNJLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQztRQUVqQixjQUFHLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRTtZQUNaLEtBQUssQ0FBQyxXQUFXLEdBQUcsa0JBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUkscUNBQXVCLENBQUMsRUFBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxFQUFDLENBQUMsQ0FBQztZQUM1RixLQUFLLENBQUMsS0FBSyxHQUFHLGtDQUF3QixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBRXJELEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFFakQsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBRUgsY0FBRyxDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUU7WUFDbEIsS0FBSyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDMUIsS0FBSyxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUM7Z0JBQzFCLE1BQU0sRUFBRSxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRTtnQkFDdkMsSUFBSSxFQUFFLEtBQUssQ0FBQyxNQUFNLEVBQUU7YUFDdkIsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2QsQ0FBQyxDQUFDLENBQUM7UUFFSCxtREFBbUQ7UUFDbkQsZ0VBQWdFO1FBQ2hFLHVGQUF1RjtRQUN2RixtQ0FBbUM7UUFDbkMsbURBQW1EO1FBQ25ELHlCQUF5QjtRQUN6QixpQkFBaUI7UUFDakIsYUFBYTtRQUViLElBQUksQ0FBQyxXQUFXLEdBQUc7WUFDZixPQUFPLEVBQUUsT0FBTyxDQUFDLFFBQVEsSUFBSSxPQUFPO1lBQ3BDLFNBQVMsRUFBRSxPQUFPLENBQUMsUUFBUSxJQUFJLE9BQU87WUFDdEMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxRQUFRLElBQUksUUFBUTtTQUN0QyxDQUFDO0lBQ04sQ0FBQztJQUVNLDhCQUFJLEdBQVg7UUFDSSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixjQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDZixDQUFDO0lBRU0scUNBQVcsR0FBbEI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztJQUM3QixDQUFDO0lBRU0sK0JBQUssR0FBWjtRQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFTSxpQ0FBTyxHQUFkO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVNLG1DQUFTLEdBQWhCO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVTLHFDQUFXLEdBQXJCO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxrQkFBVyxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2pELENBQUM7SUFFUyxxQ0FBVyxHQUFyQjtRQUNJLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQztRQUNqQixrQkFBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxLQUFLLEVBQUUsSUFBSTtZQUNyQyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDOUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxrQkFBTyxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxLQUFLLEVBQUUsSUFBSTtRQUUxQyxDQUFDLENBQUMsQ0FBQztRQUVILGtCQUFPLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFVLEtBQUssRUFBRSxJQUE0QjtZQUM3RCxLQUFLLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkMsQ0FBQyxDQUFDLENBQUM7UUFFSCxrQkFBTyxDQUFDLEVBQUUsQ0FBQyxVQUFVLEVBQUUsVUFBVSxLQUFLLEVBQUUsSUFBK0I7WUFDbkUsS0FBSyxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzFDLEtBQUssQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM1QyxDQUFDLENBQUMsQ0FBQztRQUVILGtCQUFPLENBQUMsRUFBRSxDQUFDLFVBQVUsRUFBRSxVQUFVLEtBQUssRUFBRSxJQUFJO1lBQ3hDLEtBQUssQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxjQUFjLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0wsc0JBQUM7QUFBRCxDQUFDLEFBckhELElBcUhDO0FBckhZLDBDQUFlIn0=