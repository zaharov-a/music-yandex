import {remote, app} from 'electron';
import fs = require('fs');
import path = require('path');

export interface IMainProcessStoreData {
    window: {
        width: number,
        height: number,
        url: string,
        display: number,
        x: number,
        y: number,
    },
    mini: boolean,
}

export class MainProcess_Store {
    protected static _self: MainProcess_Store;
    protected _userDataPath: string;
    protected _data: IMainProcessStoreData = {
        window: {
            width: 900,
            height: 600,
            url: 'https://music.yandex.ru/artist/1529749',
            display: 0,
            x: null,
            y: null,
        },
        mini: false,
    };

    public static getInstance() {
        return MainProcess_Store._self ? MainProcess_Store._self : (MainProcess_Store._self = new MainProcess_Store());
    }

    public setProperties(data: {}) {
        this._merge(this._data, data);

        return this;
    }

    public getProperties() {
        return this._data;
    }

    public get(name: string): any {
        return this._data[name];
    }

    public save() {
        fs.writeFileSync(this._userDataPath, JSON.stringify(this._data));

        return this;
    }

    protected constructor() {
        this._userDataPath = path.join((app || remote.app).getPath('userData'), 'config.json');
        this._parseDataFile();
    }

    protected _parseDataFile() {
        try {
            let data = JSON.parse((fs.readFileSync(this._userDataPath)).toString());
            this.setProperties(data);
        } catch (error) {

        }
    }

    protected _merge(a: {[property: string]: any}, b: {[property: string]: any}) {
        for (let key in b) {
            if (a[key] === undefined) {
                continue;
            }

            if (typeof a[key] === 'object' && a[key] !== null) {
                this._merge(a[key], b[key]);
            } else {
                a[key] = b[key];
            }
        }
    }

}