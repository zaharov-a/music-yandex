declare var externalAPI: {
    on(event: string, callback: () => void): void;
    isPlaying(): boolean;
    play(index?: number): void;
    next(): void;
    prev(): void;
    toggleMute(state?: boolean): void;
    togglePause(state?: boolean): void;
    toggleShuffle(state?: boolean): void;
    toggleRepeat(state?: boolean): void;
    getVolume(): number;
    setVolume(value: number): void;
    getProgress(): IExternalAPI_Progress;
    getCurrentTrack(): IExternalAPI_TrackInfo;
    getShuffle(): boolean;
    getRepeat(): boolean | number;
    EVENT_STATE: 'state';
    EVENT_TRACK: 'track';
    EVENT_VOLUME: 'volume',
    EVENT_PROGRESS: 'progress';
    EVENT_CONTROLS: 'controls';
    SHUFFLE_ON: true;
    SHUFFLE_OFF: false;
    REPEAT_NONE: false;
    REPEAT_ALL: true;
    REPEAT_ONE: 1;
};

import {IpcRenderer} from 'electron';
import {IExternalAPI_Progress, IExternalAPI_TrackInfo} from './interfaces';

export class ExternalAPI_Renderer {
    protected _ipc: IpcRenderer;
    protected _notifications: {[property: string]: Notification} = {};
    protected _timers: {[property: string]: number} = {};

    public constructor(ipc: IpcRenderer) {
        this._ipc = ipc;
    }

    public bind() {
        let _self = this;

        externalAPI.on(externalAPI.EVENT_STATE, function () {
            _self._send(externalAPI.EVENT_STATE, externalAPI.isPlaying());
            //            if (externalAPI.isPlaying()) {
            //                _self._notificationPlay();
            //            }
        });

        externalAPI.on(externalAPI.EVENT_VOLUME, function () {
            _self._send(externalAPI.EVENT_VOLUME, externalAPI.getVolume());
        });

        externalAPI.on(externalAPI.EVENT_TRACK, function () {
            let track: IExternalAPI_TrackInfo = externalAPI.getCurrentTrack();
            //            if (externalAPI.isPlaying()) {
            //                _self._notificationPlay();
            //            }
            _self._send(externalAPI.EVENT_TRACK, track);
        });

        externalAPI.on(externalAPI.EVENT_CONTROLS, function () {
            _self._send(externalAPI.EVENT_CONTROLS, {
                shuffle: externalAPI.getShuffle(),
                repeat: externalAPI.getRepeat(),
            });
        });

        externalAPI.on(externalAPI.EVENT_PROGRESS, function () {
            let progress = externalAPI.getProgress();
            _self._send(externalAPI.EVENT_PROGRESS, progress.position / progress.duration);
        });
    }

    protected _send(event: string, data: any) {
        this._ipc.send(event, data);
    }

//    protected _notificationPlay() {
//        let _self = this;
//        let track: IExternalAPI_TrackInfo = externalAPI.getCurrentTrack();
//
//        if (this._notifications['play']) {
//            this._notifications['play'].close();
//        }
//        if (this._timers['play']) {
//            window.clearTimeout(this._timers['play']);
//        }
//
//        this._notifications['play'] = new Notification('Music.Yandex', {
//            body: `${track.title} - ${track.artists[0].title}`,
//            icon: 'https://' + track.cover.replace('%%', '80x80'),
//            silent: true,
//        });
//
//        this._notifications['play'].onclose = function () {
//            _self._notifications['play'] = null;
//        };
//
//        this._timers['play'] = window.setTimeout(function () {
//            _self._notifications['play'].close();
//            _self._notifications['play'] = null;
//        }, 10000);
//    }
}