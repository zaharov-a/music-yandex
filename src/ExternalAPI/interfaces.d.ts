import {IpcRenderer} from 'electron';
interface IExternalAPI_ArtistInfo {
    title: string,
    link: string,
    cover: IExternalAPI_CoverInfo,
}
interface IExternalAPI_AlbumInfo {

}
//30x30, 50x50, 80x80, 100x100, 200x200, 300x300, 400x400
interface IExternalAPI_CoverInfo extends String {

}
export interface IExternalAPI_ControlsInfo {
    shuffle: boolean,
    repeat: boolean | number,
}
export interface IExternalAPI_TrackInfo {
    title: string,
    link: string,
    duration: number,
    liked: boolean,
    disliked: boolean,
    artists: IExternalAPI_ArtistInfo[];
    version: string,
    album: IExternalAPI_AlbumInfo,
    cover: IExternalAPI_CoverInfo,
}
interface IExternalAPI_Progress {
    duration: number,
    loaded: number,
    position: number,
}