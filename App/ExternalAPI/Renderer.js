"use strict";
exports.__esModule = true;
var ExternalAPI_Renderer = /** @class */ (function () {
    function ExternalAPI_Renderer(ipc) {
        this._notifications = {};
        this._timers = {};
        this._ipc = ipc;
    }
    ExternalAPI_Renderer.prototype.bind = function () {
        var _self = this;
        externalAPI.on(externalAPI.EVENT_STATE, function () {
            _self._send(externalAPI.EVENT_STATE, externalAPI.isPlaying());
            //            if (externalAPI.isPlaying()) {
            //                _self._notificationPlay();
            //            }
        });
        externalAPI.on(externalAPI.EVENT_VOLUME, function () {
            _self._send(externalAPI.EVENT_VOLUME, externalAPI.getVolume());
        });
        externalAPI.on(externalAPI.EVENT_TRACK, function () {
            var track = externalAPI.getCurrentTrack();
            //            if (externalAPI.isPlaying()) {
            //                _self._notificationPlay();
            //            }
            _self._send(externalAPI.EVENT_TRACK, track);
        });
        externalAPI.on(externalAPI.EVENT_CONTROLS, function () {
            _self._send(externalAPI.EVENT_CONTROLS, {
                shuffle: externalAPI.getShuffle(),
                repeat: externalAPI.getRepeat()
            });
        });
        externalAPI.on(externalAPI.EVENT_PROGRESS, function () {
            var progress = externalAPI.getProgress();
            _self._send(externalAPI.EVENT_PROGRESS, progress.position / progress.duration);
        });
    };
    ExternalAPI_Renderer.prototype._send = function (event, data) {
        this._ipc.send(event, data);
    };
    return ExternalAPI_Renderer;
}());
exports.ExternalAPI_Renderer = ExternalAPI_Renderer;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUmVuZGVyZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvRXh0ZXJuYWxBUEkvUmVuZGVyZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUErQkE7SUFLSSw4QkFBbUIsR0FBZ0I7UUFIekIsbUJBQWMsR0FBdUMsRUFBRSxDQUFDO1FBQ3hELFlBQU8sR0FBaUMsRUFBRSxDQUFDO1FBR2pELElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO0lBQ3BCLENBQUM7SUFFTSxtQ0FBSSxHQUFYO1FBQ0ksSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBRWpCLFdBQVcsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRTtZQUNwQyxLQUFLLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsV0FBVyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7WUFDOUQsNENBQTRDO1lBQzVDLDRDQUE0QztZQUM1QyxlQUFlO1FBQ25CLENBQUMsQ0FBQyxDQUFDO1FBRUgsV0FBVyxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFO1lBQ3JDLEtBQUssQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxXQUFXLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQztRQUNuRSxDQUFDLENBQUMsQ0FBQztRQUVILFdBQVcsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRTtZQUNwQyxJQUFJLEtBQUssR0FBMkIsV0FBVyxDQUFDLGVBQWUsRUFBRSxDQUFDO1lBQ2xFLDRDQUE0QztZQUM1Qyw0Q0FBNEM7WUFDNUMsZUFBZTtZQUNmLEtBQUssQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNoRCxDQUFDLENBQUMsQ0FBQztRQUVILFdBQVcsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRTtZQUN2QyxLQUFLLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUU7Z0JBQ3BDLE9BQU8sRUFBRSxXQUFXLENBQUMsVUFBVSxFQUFFO2dCQUNqQyxNQUFNLEVBQUUsV0FBVyxDQUFDLFNBQVMsRUFBRTthQUNsQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztRQUVILFdBQVcsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRTtZQUN2QyxJQUFJLFFBQVEsR0FBRyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDekMsS0FBSyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsY0FBYyxFQUFFLFFBQVEsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ25GLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVTLG9DQUFLLEdBQWYsVUFBZ0IsS0FBYSxFQUFFLElBQVM7UUFDcEMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUE0QkwsMkJBQUM7QUFBRCxDQUFDLEFBMUVELElBMEVDO0FBMUVZLG9EQUFvQiJ9